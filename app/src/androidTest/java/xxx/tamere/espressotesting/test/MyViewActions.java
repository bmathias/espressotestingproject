package xxx.tamere.espressotesting.test;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.view.View;

import com.google.android.apps.common.testing.ui.espresso.UiController;
import com.google.android.apps.common.testing.ui.espresso.ViewAction;

import org.hamcrest.Matcher;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import static com.google.android.apps.common.testing.ui.espresso.matcher.ViewMatchers.isAssignableFrom;
import static com.google.android.apps.common.testing.ui.espresso.matcher.ViewMatchers.isDisplayed;
import static org.hamcrest.CoreMatchers.allOf;

public class MyViewActions implements ViewAction {


    private String pictureName;

    public MyViewActions(String pictureName) {
           this.pictureName = pictureName;
    }

    public void setPictureName(String pictureName) {
        this.pictureName = pictureName;
    }

    public void takeScreenshot(View view) {

        Bitmap b = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);

        view.draw(c);

        FileOutputStream out = null;
        try {

            File file = new File("sdcard/selfies/" + System.currentTimeMillis() + "_" + pictureName + ".png");

            out = new FileOutputStream(file);
            b.compress(Bitmap.CompressFormat.PNG, 90, out);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public Matcher<View> getConstraints() {
        return allOf(isDisplayed(), isAssignableFrom(View.class));
    }

    @Override
    public String getDescription() {
        return "take screenshot";
    }

    @Override
    public void perform(UiController uiController, View view) {
        takeScreenshot(view);
    }
}

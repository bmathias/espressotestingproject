package xxx.tamere.espressotesting.test;

import android.test.ActivityInstrumentationTestCase2;

import com.squareup.spoon.Spoon;

import static com.google.android.apps.common.testing.ui.espresso.Espresso.onView;
import static com.google.android.apps.common.testing.ui.espresso.action.ViewActions.click;
import static com.google.android.apps.common.testing.ui.espresso.action.ViewActions.typeText;
import static com.google.android.apps.common.testing.ui.espresso.assertion.ViewAssertions.matches;
import static com.google.android.apps.common.testing.ui.espresso.matcher.ViewMatchers.isRoot;
import static com.google.android.apps.common.testing.ui.espresso.matcher.ViewMatchers.withText;
import static com.google.android.apps.common.testing.ui.espresso.matcher.ViewMatchers.withId;

import xxx.tamere.espressotesting.MainActivity;
import xxx.tamere.espressotesting.R;

public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {
    public MainActivityTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        getActivity();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testClick() {
        // Take a screenshot manualy
        onView(isRoot()).perform(new MyViewActions("just_launch"));

        Spoon.screenshot(getActivity(), "initial_state");

        onView(withId(R.id.textView))
                .check(matches(withText(R.string.hello_world)));

        onView(withId(R.id.editText))
                .perform(typeText("Peter"));

        Spoon.screenshot(getActivity(), "initial_state");

        onView(withId(R.id.button))
                .perform(click());

        onView(withId(R.id.textView))
                .check(matches(withText("Hello, Peterr!")));

        Spoon.screenshot(getActivity(), "initial_state");
    }
}

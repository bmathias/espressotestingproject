
# Espresso Example

This example app shows you how to use Espresso with Gradle and Android project.


	$ ./gradlew clean
	$ ./gradlew connectedInstrumentTest
	$ open app/build/reports/instrumentTests/connected/index.html
	
	
Refresh the HTML report everytime you `./gradlew connectedInstrumentTest`.

# Test With Spoon
1. compile project
----------------------
> ./gradlew assembleDebug

2. compile tests
-----------------------
> ./gradlew assembleDebugTest

3. Launch Spoon with the two apk and android SDK
-----------------------------------
**example :** 
>java -jar ../spoon-runner-1.1.1-jar-with-dependencies\ 11.23.24.jar 
--apk app/build/outputs/apk/app-debug-unaligned.apk
--test-apk app/build/outputs/apk/app-debug-test-unaligned.apk
--sdk ~/Documents/Projet/Tools/android-sdk-macosx/

4. The result is in spoon-output
-----------------
**Options :**
>    --apk               Application APK
    --fail-on-failure   Non-zero exit code on failure
    --output            Output path
    --sdk               Path to Android SDK
    --test-apk          Test application APK
    --title             Execution title
    --class-name        Test class name to run (fully-qualified)
    --method-name       Test method name to run (must also use --class-name)